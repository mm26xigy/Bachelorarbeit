from Extractors.FeatureInterface import FeatureInterface
import SyscallSingleton


class SyscallHistogram(FeatureInterface):
    def __init__(self):
        super().__init__()
        self.t_dict = {}

    def get_value(self):
        return {'syscall_histogram': self.t_dict}

    def add_syscall(self, syscall):
        if syscall.direction == ">":
            try:
                a = SyscallSingleton.SyscallSingleton.instance()
                a.add_to_dict(syscall.syscall)
                self.t_dict[syscall.syscall] += 1
                if self.max is None or self.t_dict[syscall.syscall] > self.max:
                    self.max = self.t_dict[syscall.syscall]
            except KeyError:
                self.t_dict[syscall.syscall] = 1

    def to_string(self):
        return 'syscall_histogram'
