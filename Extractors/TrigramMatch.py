from Types import Syscall
from Metrics import Metrics
from typing import List
from Extractors.FeatureInterface import FeatureInterface
from sklearn.cluster import DBSCAN
import numpy as np


class TrigramMatch(FeatureInterface):
    def __init__(self):
        super().__init__()
        self.syscall_list = []
        self.window_list = []
        self.clusters = None
        self.syscall_dict = {}
        self.dict_ctr = 0

    def add_syscall(self, syscall: Syscall):
        if syscall.direction == '>':
            try:
                self.syscall_list.append(self.syscall_dict[syscall.syscall])
            except KeyError:
                self.syscall_dict[syscall.syscall] = self.dict_ctr
                self.dict_ctr += 1
                self.syscall_list.append(self.dict_ctr)



    def finalize_window(self):
        self.window_list.append(self.syscall_list)
        self.syscall_list = []

    def get_values(self):
        if self.clusters is None:
            matrix = np.zeros([len(self.window_list), len(self.window_list)], float)
            for idx, window_x in enumerate(self.window_list):
                for idy, window_y in enumerate(self.window_list[idx+1:]):
                    matrix[idx][idy+idx+1] = Metrics.Metrics.trigram_metric(window_x, window_y)
            matrix = matrix + matrix.T - np.diag(np.diag(matrix))
            dbscan = DBSCAN(eps=0.1, n_jobs=-1, metric='precomputed', min_samples=2)
            clusters: List[int] = dbscan.fit_predict(matrix)
            self.max = max(clusters)
            self.min = min(clusters)
            self.clusters = clusters
        return self.clusters

    def to_string(self):
        return 'trigram_match'