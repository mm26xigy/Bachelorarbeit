from Types.Syscall import Syscall
from Extractors.FeatureInterface import FeatureInterface


class SyscallTiming(FeatureInterface):
    def __init__(self, _mode: str):
        self.mode = _mode
        super().__init__()
        self.counter = 0
        self.adder = 0
        self.time_max = 0
        self.time_min = 10000
        self.last_timestamp = None

    def add_syscall(self, syscall: Syscall):
        if self.last_timestamp is not None:
            if self.mode == 'min':
                diff = syscall.timestamp - self.last_timestamp
                if diff < self.time_min:
                    self.time_min = diff
            elif self.mode == 'max':
                diff = syscall.timestamp - self.last_timestamp
                if diff > self.time_max:
                    self.time_max = diff
            elif self.mode == 'avg':
                self.counter += 1
                self.adder += syscall.timestamp - self.last_timestamp
        self.last_timestamp = syscall.timestamp

    def get_value(self):
        if self.mode == 'avg':
            ret = self.adder / self.counter
            self.max = ret
            return {'syscall_timing_avg': ret}

        if self.mode == 'max':
            self.max = self.time_max
            return {'syscall_timing_max': self.time_max}
        if self.mode == 'min':
            self.max = self.time_min
            return {'syscall_timing_min': self.time_min}

    def to_string(self):
        return 'syscall_timing_' + self.mode
