from Extractors.FeatureInterface import FeatureInterface
import TrigramSingleton


class TrigramHistogram(FeatureInterface):
    def __init__(self):
        super(TrigramHistogram, self).__init__()
        self.syscall_list = []
        self.histo = {}

    def add_syscall(self, syscall):
        if syscall.direction == '>':
            self.syscall_list.append(syscall.syscall)

    def get_value(self) -> dict:
        trigrams = [''.join(self.syscall_list[i: i + 3]) for i in range(len(self.syscall_list) - 3 + 1)]
        for i in trigrams:
            t = TrigramSingleton.TrigramSingleton.instance()
            t.add_to_dict(i)
            try:
                self.histo[i] += 1
            except KeyError:
                self.histo[i] = 1
        self.max = self.histo[max(self.histo)]
        self.min = self.histo[min(self.histo)]
        return {'trigram_histogram': self.histo}

    def to_string(self):
        return 'trigram_histogram'
