from Extractors.FeatureInterface import FeatureInterface
from Types import Syscall


class PathLength(FeatureInterface):
    def __init__(self):
        super().__init__()
        self.ret = 0
        self.count = 0

    def add_syscall(self, syscall: Syscall):
        if syscall.direction == '<' and 'open' in syscall.syscall:
            if len(syscall.arguments) > 0 and "<f>" in syscall.arguments[0]:
                self.ret += syscall.arguments[0].count("/")
                self.count += 1
        if syscall.direction == ">":
            if 'read' in syscall.syscall or "write" in syscall.syscall:
                if len(syscall.arguments) > 0 and "<f>" in syscall.arguments[0]:
                    self.ret += syscall.arguments[0].count("/")
                    self.count += 1

    def get_value(self):
        if self.count > 0:
            pl = self.ret / self.count
            self.max = pl
            return {'path_length': pl}
        else:
            return {'path_length': 0}

    def to_string(self):
        return 'path_length'
