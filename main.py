import json
import glob
import sys
import timeit

import numpy as np
import pandas as pd
import csv
from typing import List
# import wandb

import TrigramSingleton
from Extractors.TrigramHistogram import TrigramHistogram
from Extractors.TrigramMatch import TrigramMatch
from Extractors.PathLength import PathLength
from Extractors.SyscallTiming import SyscallTiming
from Extractors.SyscallHistogram import SyscallHistogram
from Types.Alarm import Alarm
from Types.Feature import Feature
from Helpers import Helpers
import SyscallSingleton
from Metrics.Metrics import Metrics

from sklearn.cluster import DBSCAN, Birch, KMeans

# TODO: In Arbeit nicht vorhanden Normalverteilung für Daten nachweisen (MinMax Normalization)
# process mining literatur cluster
#
# path min max


class Value:
    MAX = 1
    MIN = 0


class DataLoader:
    """
    path: needs to be a directory with the alarms in a seperate /overview folder
    extracts:
        # szenario:      name of the attack (as described in filename)
        # alarm_true:    boolean true if the alarm was based on an actual attack
                         (for checking and evaluation of clusters)
        # window:        list of the syscalls in the window (includes normally ignored calls)
        # window_range:  list of linenumbers to the corresponding syscalls in the window
        # records_path:  path to the file that contains the syscalls.
    """

    def __init__(self, path_to_alarms):
        super().__init__()
        self.path_alarms = path_to_alarms
        self.loaded_entities = 0
        self.filename = None
        self.total_alarms = None
        self.alarm_list = []

    def run(self):
        fn = self.path_alarms
        global filename
        with open(fn) as fd:
            filename = fn.split('/')[-1]
            file = json.load(fd)
            self.total_alarms = len(file["alarms"])
            print("Total Alarms: " + str(self.total_alarms))
            for dic in file["alarms"]:
                szenario = dic["scenario"]
                records_path = dic["filepath"]
                alarm_true = dic["correct"]
                window_range = (dic["first_line_id"], dic["last_line_id"])
                self.alarm_list.append(Alarm(szenario, alarm_true, window_range, records_path))
                self.loaded_entities += 1
        print(str(self.loaded_entities) + " Alarms loaded")


class FeatureExtractor:

    # TODO: Features aus Papers und Projekt anschauen

    def __init__(self, _alarm_list: List[Alarm], _features: List[str]):
        self.alarm_list = _alarm_list
        self.enabled_features = _features
        self.extracted_features = []
        self.syscall_histogram_enabled = "syscall_histogram" in self.enabled_features
        self.path_length_enabled = "path_length" in self.enabled_features
        self.syscall_timing_min_enabled = "syscall_timing_min" in self.enabled_features
        self.syscall_timing_max_enabled = "syscall_timing_max" in self.enabled_features
        self.syscall_timing_avg_enabled = "syscall_timing_avg" in self.enabled_features
        self.trigram_histogram_enabled = 'trigram_histogram' in self.enabled_features
        self.trigram_matching_enabled = "trigram_match" in self.enabled_features
        self.absolute_records_path = None
        self.min_max_table = {str: List}
        self.processed_alarms = 0

    def run(self):
        print('FeatureExtractor started')
        tri = None
        if self.trigram_matching_enabled:
            tri = TrigramMatch()
        for alarm in self.alarm_list:
            self.absolute_records_path = path + "/LID-DS-2021-fixed-exploit-time/" + alarm.records_path
            extractors = []
            #das kann auch dynamisch erstellt werden
            if self.syscall_histogram_enabled:
                extractors.append(SyscallHistogram())
            if self.path_length_enabled:
                extractors.append(PathLength())
            if self.syscall_timing_avg_enabled:
                extractors.append(SyscallTiming('avg'))
            if self.syscall_timing_min_enabled:
                extractors.append(SyscallTiming('min'))
            if self.syscall_timing_max_enabled:
                extractors.append(SyscallTiming('max'))
            if self.trigram_histogram_enabled:
                extractors.append(TrigramHistogram())

            for syscall in Helpers.window_iterate(alarm.window_range, self.absolute_records_path):
                for extractor in extractors:
                    extractor.add_syscall(syscall)
                if self.trigram_matching_enabled:
                    tri.add_syscall(syscall)
            ndic = {}
            if self.trigram_matching_enabled:
                tri.finalize_window()
            for extractor in extractors:   # trigram nicht in extractors
                i = extractor.get_value()
                try:
                    # extractor.max = i wenn der rückgabewert numerisch ist. Ansonsten muss extractor.max im
                    # Extractor bei get_value
                    # auf den Maximalen Wert gesetzt werden
                    min_max = self.min_max_table[extractor.to_string()]
                    if min_max[Value.MAX] < extractor.max:
                        min_max[Value.MAX] = extractor.max
                        self.min_max_table[extractor.to_string()] = min_max
                    elif min_max[Value.MIN] > extractor.max:
                        min_max[Value.MIN] = extractor.max
                        self.min_max_table[extractor.to_string()] = min_max
                except KeyError:
                    self.min_max_table[extractor.to_string()] = [extractor.max, extractor.max]
                ndic.update(i)
            features = Feature(alarm, ndic, [])
            self.extracted_features.append(features)
            self.processed_alarms += 1
            if self.processed_alarms % 200 == 0:
                print(self.processed_alarms)
        if self.trigram_matching_enabled:  #trigram muss separat gehandled werden
            print("Trigrams werden berechnet")
            _ = tri.get_values()
            self.min_max_table[tri.to_string()] = [tri.max, tri.min]
            for idx, cluster in enumerate(tri.get_values()):
                self.extracted_features[idx].feature_dict['trigram_match'] = cluster


class Normalizer:
    alarm_features: Feature

    def __init__(self, _enabled_features: List[str], _extracted_features: List[Feature], _min_max_table: {str: List}):
        self.enabled_features = _enabled_features
        self.min_max_table: {str: List[2]} = _min_max_table
        self.extracted_features = _extracted_features
        self.processed_alarms = 0
        self.enabled_features_ordered = []
        for i in _extracted_features:
            i.feature_vector = []
    #todo: hinzufügen zum Vektor außerhalb der Funktionen
    def min_max_single_value(self, feature_key, min_max):
        self.alarm_features.feature_vector.append((self.alarm_features.feature_dict[feature_key]
                                                   - min_max[Value.MIN]) /
                                                  (min_max[Value.MAX]
                                                   - min_max[Value.MIN])) if (min_max[Value.MAX]
                                                   - min_max[Value.MIN]) else self.alarm_features.feature_vector.append(0)

    def normalize_histogram(self, feature_key, min_max: List):
        temp = {}
        for key in self.alarm_features.feature_dict[feature_key]:
            temp[key] = self.alarm_features.feature_dict[feature_key][key] / min_max[Value.MAX]
        if feature_key == 'syscall_histogram':
            inst = SyscallSingleton.SyscallSingleton.instance()
        elif feature_key == 'trigram_histogram':
            inst = TrigramSingleton.TrigramSingleton.instance()
        else:
            return
        for i in inst.syscalls:
            try:
                self.alarm_features.feature_vector.append(temp[i])
            except KeyError:
                self.alarm_features.feature_vector.append(0)

    def normalize_path_length(self, key, min_max: List):
        if self.alarm_features.path_length > 0:
            self.alarm_features.feature_vector.append(self.alarm_features.path_length / min_max[Value.MAX])

    def run(self):

        func_table = {'syscall_histogram': self.normalize_histogram,
                      'path_length': self.min_max_single_value,
                      'syscall_timing_min': self.min_max_single_value,
                      'syscall_timing_max': self.min_max_single_value,
                      'syscall_timing_avg': self.min_max_single_value,
                      'trigram_match': self.min_max_single_value,
                      'trigram_histogram': self.normalize_histogram}

        for index, _alarm_features in enumerate(self.extracted_features):
            self.alarm_features = _alarm_features
            for key in _alarm_features.feature_dict:
                if key in self.enabled_features:
                    temp: List[2] = self.min_max_table[key]
                    func_table[key](key, temp)
                    if key not in self.enabled_features_ordered:
                        self.enabled_features_ordered.append(key)
            self.extracted_features[index] = self.alarm_features
            self.processed_alarms += 1


class Clusterer:

    def __init__(self,
                 _normalized_features: List[Feature],
                 _sorted_enabled_features,
                 eval_file,
                 method: str = 'birch',
                 visualize: bool = False,
                 console_output = True,
                 dimension: int = 2,
                 eps: float = 0.1,
                 min_samples: int = 5):
        self.console_output = console_output
        self.sorted_enabled_features = _sorted_enabled_features
        self.normalized_features = _normalized_features
        self.eps = eps
        self.eval_file = eval_file
        self.processed_alarms = 0
        self.min_samples = min_samples
        self.dbscan = DBSCAN(n_jobs=-1, eps=eps, min_samples=min_samples)
        self.birch = Birch(n_clusters=5, threshold=eps)
        self.kmeans = KMeans(n_clusters=5)
        self.vis = visualize
        self.dim = dimension
        self.method = method

    def run(self):
        X = []
        all_alarms = []
        for feature_vector in self.normalized_features:
            all_alarms.append(feature_vector.original_alarm)
            X.append(feature_vector.feature_vector)
            self.processed_alarms += 1
        # todo: alarme ordentlich in df packen für analyse
        X = np.array(X)
        model = None
        if self.method == 'dbscan':
            model = self.dbscan
            clusters = self.dbscan.fit_predict(X)
        elif self.method == 'kmeans':
            model = self.kmeans
            clusters = self.kmeans.fit_predict(X)
        elif self.method == 'birch':
            model = self.birch
            clusters = self.birch.fit_predict(X)
        else:
            print('No valid method given. Please use kmeans, dbscan or birch')
            return
        df_evaluation = pd.DataFrame()
        df_evaluation["feature_vector"] = [feature_vector for feature_vector in X]
        df_evaluation["cluster"] = clusters
        # df_evaluation.to_pickle(f'eval_{filename}.{"-".join(self.sorted_enabled_features)}.pkl')
        column_names = []
        for feature_name in self.sorted_enabled_features:
            if feature_name == 'syscall_histogram':
                inst = SyscallSingleton.SyscallSingleton.instance()
                column_names.extend([i for i in inst.syscall_names])
            elif feature_name == 'trigram_histogram':
                inst = TrigramSingleton.TrigramSingleton.instance()
                column_names.extend([i for i in inst.syscall_names])
            else:
                column_names.append(feature_name)

        df_features = pd.DataFrame(X, columns=column_names)

        df_all_attacks = [x.was_attack for x in all_alarms]
        df_features['was_attack'] = df_all_attacks
        df_alarm_cluster = pd.DataFrame(df_all_attacks, columns=["was_attack"], dtype=object)
        df_alarm_cluster["Cluster"] = clusters
        df_all_attacks = df_alarm_cluster[df_alarm_cluster["was_attack"] == True]
        # wandb.sklearn.plot_clusterer(model, X, clusters, labels=None, model_name=self.method)
        for num_cluster in df_alarm_cluster["Cluster"].unique():
            total_alarms_in_cluster = df_alarm_cluster[df_alarm_cluster["Cluster"] == num_cluster].count()[0]
            percentage_normal_behavior = (1 - (df_all_attacks[df_all_attacks["Cluster"] == num_cluster].count()[0] /
                                               total_alarms_in_cluster)) * 100
            if self.console_output:
                print(
                    f"{str(percentage_normal_behavior)} % of Cluster {str(num_cluster)}({str(total_alarms_in_cluster)})"
                    f"were normal behavior")

        ptc = Metrics.percent_fully_classified(df_alarm_cluster, df_all_attacks)
        '''wandb.log({"percent_classified": ptc, "total_alarms": len(df_alarm_cluster),
                   "false_alarms": len(df_all_attacks),
                   "true_alarms": len(df_alarm_cluster) - len(df_all_attacks),
                   "clusters": len(df_alarm_cluster["Cluster"].unique()),
                   "method": self.method,
                   "min_samples": self.min_samples,
                   "eps": self.eps,
                   "szenario": self.eval_file.replace(".csv", "")})'''
        if self.console_output:
            print(f"The percentage of Normal_Behavior is at {(1-(len(df_all_attacks) / len(df_alarm_cluster))) * 100}%")
        with open(self.eval_file, 'a', newline='') as cs:
            w = csv.writer(cs)
            wr = [', '.join(self.sorted_enabled_features), ptc, self.eps, self.min_samples, len(df_alarm_cluster["Cluster"].unique())]
            w.writerow(wr)
        df_alarm_cluster.to_pickle(f"{self.eval_file}.pkl")
        if self.vis:
            VisualizerXd(self.sorted_enabled_features, df_features, clusters, dimensions=self.dim, method="tsne")


class VisualizerXd:
    def __init__(self, _sorted_enabled_features, df: pd.DataFrame, clusters: List, dimensions: int, method: str):
        from sklearn.decomposition import PCA
        from sklearn.manifold import TSNE
        import plotly.express as px
        self.sorted_enabled_features = _sorted_enabled_features
        self._perplexity = 50
        z_coord = None

        if method == "PCA":
            pca_xd = PCA(n_components=dimensions)
            pcas_xd = pd.DataFrame(pca_xd.fit_transform(df.drop('was_attack')))
            pcas_xd.columns = ["PC" + str(i) + "_" + str(dimensions) + "d" for i in range(0, dimensions)]
            vis_frame = pd.concat([df, pcas_xd], axis=1, join="inner")
            x_coord = pcas_xd.columns[0]
            y_coord = pcas_xd.columns[1]
            if dimensions == 3:
                z_coord = pcas_xd.columns[2]
        elif len(df.columns) - 1 < 4:
            dimensions = len(df.columns) - 1
            vis_frame = df
            x_coord = self.sorted_enabled_features[0]
            y_coord = self.sorted_enabled_features[1]
            if dimensions == 3:
                z_coord = self.sorted_enabled_features[2]

        else:
            # TSNE Parameter (https://distill.pub/2016/misread-tsne/
            tsne_xd = TSNE(n_components=dimensions, perplexity=self._perplexity, init='random', learning_rate='auto')
            pcas_xd = pd.DataFrame(tsne_xd.fit_transform(df.drop('was_attack', axis=1)))
            pcas_xd.columns = ["PC" + str(i) + "_" + str(dimensions) + "d" for i in range(0, dimensions)]
            vis_frame = pd.concat([df, pcas_xd], axis=1, join="inner")
            x_coord = pcas_xd.columns[0]
            y_coord = pcas_xd.columns[1]
            if dimensions > 2:
                z_coord = pcas_xd.columns[2]

        vis_frame["Cluster"] = clusters
        if dimensions == 2:
            fig = px.scatter(vis_frame,
                             x=x_coord,
                             y=y_coord,
                             color="Cluster",
                             symbol='was_attack')
        else:
            fig = px.scatter_3d(vis_frame,
                                x=x_coord,
                                y=y_coord,
                                z=z_coord,
                                color="Cluster",
                                symbol='was_attack')
        fig.show()


class Tester:
    def __init__(self, path_to_alarm_file):
        temp = ['dbscan', 'kmeans', 'birch']
        eps_value = 0.2
        all_features = ['path_length', 'syscall_timing_max', 'syscall_timing_min',
                          'syscall_timing_avg', 'syscall_histogram','trigram_histogram', 'trigram_match']
        try:
            szenario = path_to_alarm_file.split('/')[-1].split('.')[0]
        except:
            szenario = 'unknown'
        for i in temp:
            with open(f'{i}/eval_{szenario}.csv', 'w', newline='') as cs:
                w = csv.writer(cs)
                wr = ['enabled_features', 'percent_classified', 'eps', 'min_samples', 'clusters']
                w.writerow(wr)
        dl = DataLoader(path_to_alarm_file)
        dl.run()
        fe = FeatureExtractor(dl.alarm_list, all_features)
        fe.run()
        print('Feature Extraction Done...\nTesting Starts now.')
        all_combinations = [i for i in Helpers.powerset(all_features)]
        c = 1
        combination = ['syscall_histogram', "trigram_match"]
        if len(combination) > 0:
            no = Normalizer(combination, fe.extracted_features, fe.min_max_table)
            no.run()
            for i_clustermethod in temp:
                if i_clustermethod == 'dbscan':
                    for i in range(2,10):
                        cl = Clusterer(no.extracted_features, no.enabled_features_ordered, f'{i_clustermethod}/eval_{szenario}.csv',
                                       eps=eps_value, console_output=True, min_samples=i, method=i_clustermethod)
                        cl.run()
                else:
                    cl = Clusterer(no.extracted_features, no.enabled_features_ordered,
                                   f'{i_clustermethod}/eval_{szenario}.csv',
                                   eps=eps_value, console_output=True, min_samples=None, method=i_clustermethod)
                    cl.run()

        c += 1
        pass


if __name__ == '__main__':
    # wandb.init(project="my-test-project")
    path = sys.argv[1]
    path_to_alarm_files = sys.argv[2]
    index = sys.argv[3]
    mode = 'test'
    start = timeit.default_timer()
    # path = "/Users/max/LIDDS"
    # filename = '/Users/max/LIDDS/overview/alarms_n_3_w_100_t_False_LID-DS-2021-sorted_CVE-2012-2122.json'
    path_to_alarm = glob.glob(f'{path_to_alarm_files}/*')[int(index)]
    # path_to_alarm = f'{path_to_alarm_files}/{files_all[int(index)].split("/")[-1]}'
    path_to_alarm = f"{path_to_alarm_files}/alarms_n_7_w_100_t_False_LID-DS-2021-sorted_Bruteforce_CWE-307.json"
    print(path_to_alarm)
    if mode == 'test':
        te = Tester(path_to_alarm)
    else:
        dl = DataLoader("/Users/max/LIDDS/overview/alarms_n_3_w_100_t_False_LID-DS-2021-sorted_CVE-2012-2122.json")
        dl.run()
        fe = FeatureExtractor(dl.alarm_list, ['syscall_histogram', 'path_length', 'syscall_timing_max', 'trigram_match'])
        fe.run()
        no = Normalizer(fe.enabled_features, fe.extracted_features, fe.min_max_table)
        no.run()
        cl = Clusterer(no.extracted_features, no.enabled_features_ordered, 'eval_csv',
                       visualize=True,
                       dimension=3,
                       eps=0.3,
                       console_output=True,
                       min_samples=8,
                       method='birch')
        cl.run()
    end = timeit.default_timer()
    print("Done after:")
    print(str(end - start) + "s")

