from typing import List

class Metrics:
    @staticmethod
    def trigram_metric(x: List[str], y: List[str]):
        trigrams_x = set([''.join(str(f) for f in x[i: i+3]) for i in range(len(x)-3+1)])
        trigrams_y = set([''.join(str(f) for f in y[i: i+3]) for i in range(len(y)-3+1)])
        unique = len(trigrams_x | trigrams_y)
        equal = len(trigrams_x & trigrams_y)
        return 1 - round(float(equal) / float(unique), 6)

    @staticmethod
    def percent_fully_classified(df_full, df_true):
        total_alarms = len(df_full)
        p = 0
        for num_cluster in df_full["Cluster"].unique():
            total_alarms_in_cluster = df_full[df_full["Cluster"] == num_cluster].count()[0]
            percentage_normal_behavior = (1 - (df_true[df_true["Cluster"] == num_cluster].count()[0] /
                                               total_alarms_in_cluster)) * 100
            if percentage_normal_behavior == 0 or percentage_normal_behavior == 100:
                p += total_alarms_in_cluster
        return p / total_alarms