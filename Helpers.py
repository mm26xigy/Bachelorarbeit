import zipfile
from Types.Syscall import Syscall
from itertools import chain, combinations



class Helpers:
    @staticmethod
    def window_iterate(syscall_window, records_path):
        (index_start, index_end) = syscall_window
        i = index_start
        try:
            temp = open(records_path,'r')
        except FileNotFoundError:
            print(f'File not found: {records_path}\nPlease download file and press enter.')
            input()
        finally:
            temp.close()

        with zipfile.ZipFile(records_path, 'r') as zipped:
            with zipped.open(records_path.split('/')[-1].replace('.zip', '.sc')) as unzipped:
                for _, line in enumerate(unzipped, start=index_start):
                    if i > index_end:
                        break
                    yield Syscall(line.decode('UTF-8').replace(" \n", ""))






    @staticmethod
    def sizeof_fmt(num, suffix='B'):
        for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)

    @staticmethod
    def powerset(iterable):
        "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
        s = list(iterable)
        return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))