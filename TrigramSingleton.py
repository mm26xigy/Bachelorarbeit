from SyscallSingleton import Singleton

@Singleton
class TrigramSingleton:
    def __init__(self):
        self.syscall_names = []
        self.syscalls: dict = {}
        self.counter = 0

    def add_to_dict(self, syscall_entity: str):
        if syscall_entity not in self.syscalls:
            self.syscalls[syscall_entity] = self.counter
            self.syscall_names.append(syscall_entity)
            self.counter += 1

