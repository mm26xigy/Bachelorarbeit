from dataclasses import dataclass

@dataclass
class Alarm:
    scenario: str
    was_attack: bool
    window_range: tuple
    records_path: str
