from Types import Alarm
from typing import List


class Feature:
    def __init__(self, _original_alarm: Alarm, _feature_dict: dict, _feature_vector: List[float]):
        self.original_alarm = _original_alarm
        self.feature_dict = _feature_dict
        self.feature_vector = _feature_vector
        #Todo: werden nicht mehr benutzt, sollten zumindest nicht benutzt werden
        self.syscall_histogram: dict = _feature_dict.get('syscall_histogram', None)
        self.path_length: int = _feature_dict.get('path_length', None)
        self.syscall_timing_min: float = _feature_dict.get('syscall_timing_min', None)
        self.syscall_timing_max: float = _feature_dict.get('syscall_timing_max', None)
        self.syscall_timing_avg: float = _feature_dict.get('syscall_timing_avg', None)
        self.trigram_match: float = _feature_dict.get('trigram_match', None)

