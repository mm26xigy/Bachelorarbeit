from typing import List


class Syscall:
    def __init__(self, call: str):
        splitter = call.split(" ")
        self.timestamp = int(splitter[0])  # time in ns
        self.uid = splitter[2]
        self.service_name = splitter[3]
        self.thread_id: int = int(splitter[4])
        self.direction: str = splitter[6]
        self.syscall: str = splitter[5]
        self.arguments: List[str] = [splitter[i] for i in range(7, len(splitter))]