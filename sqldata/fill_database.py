import glob
import sqlite3
import csv



if __name__ == "__main__":

    con = sqlite3.connect('path/to/db')

    stat = "INSERT INTO data VALUES (?,?,?,?,?,?,?,?,?)"
    li = []
    for file in glob.glob("../eval/dbscan/*.csv"):
        name = file.split("/")[-1]
        scenario = name.split("_")[-1].split(".")[0]
        if 'CWE' in scenario:
            scenario = name.split("_")[-2] + name.split("_")[-1].split(".")[0]
        try:
            n = int(name.split("_")[3])
        except IndexError:
            print(name)
            exit(1)
        t = name.split("_")[7]
        with open(file) as csvfile:
            current_file = csv.reader(csvfile)
            next(current_file, None)
            for row in current_file:
                li.append(('dbscan', row[0], float(row[1]), float(row[2]), int(row[3]), int(row[4]), scenario, n, t))
    con.executemany(stat, li)
    con.commit()
    con.close()


