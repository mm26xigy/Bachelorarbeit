import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

# code from sklearns documentation

if __name__ == "__main__":

    np.random.seed(0)

    # ============
    # Generate datasets. We choose the size big enough to see the scalability
    # of the algorithms, but not too big to avoid too long running times
    # ============
    n_samples = 1500
    noisy_circles = datasets.make_circles(n_samples=n_samples, factor=0.5, noise=0.05)
    noisy_moons = datasets.make_moons(n_samples=n_samples, noise=0.05)
    blobs = datasets.make_blobs(n_samples=n_samples, random_state=8)
    no_structure = np.random.rand(n_samples, 2), None

    # Anisotropicly distributed data
    random_state = 170
    X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    transformation = [[0.6, -0.6], [-0.4, 0.8]]
    X_aniso = np.dot(X, transformation)
    aniso = (X_aniso, y)

    # blobs with varied variances
    varied = datasets.make_blobs(
        n_samples=n_samples, cluster_std=[1.0, 2.5, 0.5], random_state=random_state
    )

    # ============
    # Set up cluster parameters
    # ============
    fig, axs = plt.subplots(2, 3)
    axs[0,0].scatter(noisy_moons[0][:,0],noisy_moons[0][:,1], s=0.5)
    axs[0,0].set_title("Halbmonde")
    axs[0, 1].scatter(noisy_circles[0][:, 0], noisy_circles[0][:, 1], s=0.5)
    axs[0, 1].set_title("Kreise")
    axs[0, 2].scatter(blobs[0][:, 0], blobs[0][:, 1], s=0.5)
    axs[0, 2].set_title("Punkte")
    axs[1, 0].scatter(no_structure[0][:, 0], no_structure[0][:, 1], s=0.5)
    axs[1, 0].set_title("Zufall")
    axs[1, 1].scatter(aniso[0][:, 0], aniso[0][:, 1], s=0.5)
    axs[1, 1].set_title("gestreckte Punkte")
    axs[1, 2].scatter(varied[0][:, 0], varied[0][:, 1], s=0.5)
    axs[1, 2].set_title("variable Dichte")
    fig.suptitle("Beispiele für 2D-Formen", fontsize=14)

    for rows in axs:
        for ax in rows:
            ax.set_yticks([])
            ax.set_xticks([])

    # plt.savefig("bsp_2d_formen.png")
    plt.show()
